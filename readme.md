# Aplikasi Data karyawan

## Requirement
 - PHP 7.3 atau lebih
 - Composer 2.0.11

## Installation

- Clone aplikasi
- Masuk ke direktori aplikasi hasil clone
- Copy .env-example ke .env
- sesuaikan isi .env
- jalankan ```docker-compose build && docker-compose up -d```
- buat database dengan nama ```db_opsigo``` (atau menyesuaikan)
- masuk ke folder apps
- copy .env-example ke .env
- sesuaikan isi .env (terutama database)
- masuk ke container ```docker exec -ti `container` bash```
- masuk ke folder /var/www/apps
- jalankan ```composer install```

## Generate Table Database

Menggunakan migrate :

masih di dalam container, jalankan ```php artisan migrate:fresh --seed```

Manual :

restore database yang ada di path ```apps/database/db_opsigo.sql```