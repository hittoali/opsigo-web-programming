@extends('layouts.app')

@section('title', '| Karyawan')

@section('header','Karyawan')
@section('header_desc','Data Karyawan')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="" class="text-muted">Data</a>
    </li>
@endsection

@section('content')
@include ('layouts.partial.flash')

<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">@yield('header_desc')
        </div>
        <div class="card-toolbar">
            <a href="{{ route('employee.create') }}" class="btn btn-primary font-weight-bolder">
            <i class="la la-plus"></i>Tambah Karyawan</a>
        </div>
    </div>
    <div class="card-body">
    	<table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
			<thead>
				<tr>
					<th>No</th>
					<th>NIP</th>
					<th>Nama Lengkap</th>
					<th>Atasan Lansung</th>
					<th>Jumlah Bawahan</th>
					<th>Status Karyawan</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1 ?>
				@foreach ($employees as $employee)
					<tr>
						<td>{{ $no }}</td>
						<td>{{ $employee->nip }}</td>
						<td>{{ $employee->fullname }}</td>
						<td>{{ empty($employee->parent->fullname) ? "-" : $employee->parent->fullname}}</td>
						<td>{{ $employee->mapping->count() }}</td>
						<td>
							<span class="label label-lg font-weight-bold {{ generateClassStatus($employee->status->id) }} label-inline">{{ $employee->status->name }}</span></td>
						<td>
							@if($employee->employee_status_id != 3)
								<a href="{{route('employee.edit', ['id' => $employee->id])}}" class="btn btn-primary btn-sm">Edit</a>
							@endif()
						</td>
					</tr>
				<?php $no++ ?>
				@endforeach
			</tbody>
		</table>
    </div>
</div>

@endsection

@section('extend-scripts')
@endsection