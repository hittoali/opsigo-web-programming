@extends('layouts.app')

@section('title', '| Karyawan')

@section('header','Karyawan')
@section('header_desc','Data Karyawan')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="" class="text-muted">Data</a>
    </li>
    <li class="breadcrumb-item">
        <a href="" class="text-muted">Tambah</a>
    </li>
@endsection

@section('content')
@include ('layouts.partial.flash')
	<div class="card card-custom">
	    <div class="card-header">
	        <h3 class="card-title">
	            @yield('header')
	        </h3>
	    </div>
	    {!! Form::model(null, array('route' => ['employee.store'], 'class' => 'form')) !!}
	        <div class="card-body">
	        	<div class="form-group row">
                    {{ Form::label('nip', 'Nip',['class' => 'col-2 col-form-label']) }}
                    <div class="col-3">
                        {{ Form::text('nip', $nip, array('class' => 'form-control', 'readonly' => 'readonly')) }}
                        @if($errors->has('nip'))
                            <span class="help-block" style="color:red;">{{ $errors->first('nip') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('fullname', 'Nama Lengkap',['class' => 'col-2 col-form-label']) }}
                    <div class="col-3">
                        {{ Form::text('fullname', null, array('class' => 'form-control')) }}
                        @if($errors->has('fullname'))
                            <span class="help-block" style="color:red;">{{ $errors->first('fullname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('place_of_birth', 'Tempat, tanggal lahir',['class' => 'col-2 col-form-label']) }}
                    <div class="col-3">
                        {{ Form::text('place_of_birth', null, array('class' => 'form-control')) }}
                        @if($errors->has('place_of_birth'))
                            <span class="help-block" style="color:red;">{{ $errors->first('place_of_birth') }}</span>
                        @endif
                    </div>
                    <div class="col-3">
                        {{ Form::date('date_of_birth', null, array('class' => 'form-control')) }}
                        @if($errors->has('date_of_birth'))
                            <span class="help-block" style="color:red;">{{ $errors->first('date_of_birth') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('join_date', 'Tanggal Masuk',['class' => 'col-2 col-form-label']) }}
                    <div class="col-3">
                        {{ Form::date('join_date', null, array('class' => 'form-control')) }}
                        @if($errors->has('join_date'))
                            <span class="help-block" style="color:red;">{{ $errors->first('join_date') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('employee_parent_id', 'Atasan Langsung',['class' => 'col-2 col-form-label']) }}
                    <div class="col-3">
                        {!! Form::select('employee_parent_id', $parent, null, ['class' => 'form-control select2', 'id' => 'employee_parent_id']) !!}
                        @if($errors->has('employee_parent_id'))
                        <span class="help-block" style="color:red;">{{ $errors->first('employee_parent_id') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('employee_status_id', 'Status Karyawan',['class' => 'col-2 col-form-label']) }}
                    <div class="col-3">
                        {!! Form::select('employee_status_id', $status, null, ['class' => 'form-control select2', 'id' => 'employee_status_id']) !!}
                        @if($errors->has('employee_status_id'))
                        <span class="help-block" style="color:red;">{{ $errors->first('employee_status_id') }}</span>
                        @endif
                    </div>
                </div>
	        </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button type="submit" class="btn btn-success mr-2">Simpan</button> &nbsp;&nbsp;
                        <a href="{{route('employee.list')}}" class="btn btn-dark mr-2">Kembali</a>
                    </div>
                </div>
            </div>
	    {!! Form::close() !!}
	</div>
@endsection

@section('extend-scripts')
    <script>
        $('#employee_parent_id').select2({});
        $('#employee_status_id').select2({});
    </script>
@endsection