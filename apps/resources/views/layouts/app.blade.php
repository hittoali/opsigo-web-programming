<!DOCTYPE html>

<html lang="en">
    <head>
        <!-- META SECTION -->
        <meta charset="utf-8" />
        <title>{{ env('COMPANY_NAME') }} @yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="PT.Reka Jaya Prima" />
        <meta name="keywords" content="PT.Reka Jaya Prima">
        <meta name="author" content="hittoali" />
        <meta name="MobileOptimized" content="320" />

        <link href="{{url('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('assets/css/themes/layout/header/base/light.css')}}" rel="stylesheet" type="text/css" />

        <!-- EXTRA ASSETS -->
        @yield('extra-css')
        <!-- END EXTRA ASSETS -->


    </head>
    <body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
        <div class="d-flex flex-column flex-root">
            <div class="d-flex flex-row flex-column-fluid page">
                <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                    @include('layouts.partial.headpanel')
                    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                        @include('layouts.partial.subpanel')
                        <div class="d-flex flex-column-fluid">
                            <div class="container">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                    @include('layouts.partial.footer')
                </div>
            </div>
        </div>
        
        <script src="{{url('assets/plugins/global/plugins.bundle.js')}}"></script>


        @yield('extend-scripts')
    </body>
</html>