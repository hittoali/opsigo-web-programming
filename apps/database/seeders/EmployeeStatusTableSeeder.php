<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class EmployeeStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
    		['name' => 'Tetap', 'created_at' => date("Y-m-d H:i:s")],
    		['name' => 'Kontrak', 'created_at' => date("Y-m-d H:i:s")],
    		['name' => 'Keluar','created_at' => date("Y-m-d H:i:s")],
    	];

        DB::table('table_employee_status')->insert($data);
    }
}
