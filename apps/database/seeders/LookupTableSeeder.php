<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class LookupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('table_lookup')->insert([
            'type' => 'nip',
            'name' => 'format nip',
            'value' => '880001',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
