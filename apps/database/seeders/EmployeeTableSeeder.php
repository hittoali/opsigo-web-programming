<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('table_employee')->insert([
            'nip' => 'RJP880001',
            'fullname' => 'Dr Andre Syaefudin',
            'place_of_birth' => null,
            'date_of_birth' => null,
            'join_date' => null,
            'employee_status_id' => 1,
            'level' => 1,
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
