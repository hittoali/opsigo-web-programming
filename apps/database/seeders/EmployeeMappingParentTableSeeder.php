<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class EmployeeMappingParentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('table_employee_mapping_parent')->insert([
            'employee_id' => 1,
            'employee_parent_id' => null,
        ]);
    }
}
