/*
 Navicat Premium Data Transfer

 Source Server         : Opsigo
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : 192.168.99.100:43066
 Source Schema         : db_opsigo

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 17/04/2021 16:59:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2021_04_16_062343_create_table_employee', 1);
INSERT INTO `migrations` VALUES (2, '2021_04_16_062441_create_table_employee_mapping_parent', 1);
INSERT INTO `migrations` VALUES (3, '2021_04_16_062502_create_table_employee_status', 1);
INSERT INTO `migrations` VALUES (4, '2021_04_16_221726_create_table_lookup', 1);

-- ----------------------------
-- Table structure for table_employee
-- ----------------------------
DROP TABLE IF EXISTS `table_employee`;
CREATE TABLE `table_employee`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nip` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `place_of_birth` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `date_of_birth` date NULL DEFAULT NULL,
  `join_date` date NULL DEFAULT NULL,
  `employee_parent_id` int(11) NULL DEFAULT NULL,
  `list_employee_parent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `employee_status_id` bigint(20) UNSIGNED NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `table_employee_nip_unique`(`nip`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of table_employee
-- ----------------------------
INSERT INTO `table_employee` VALUES (1, 'RJP880001', 'Dr Andre Syaefudin', 'bandung', '1987-02-02', '1990-02-02', NULL, NULL, 1, 1, '2021-04-17 12:53:26', '2021-04-17 15:15:20');
INSERT INTO `table_employee` VALUES (2, 'RPJ880002', 'Asep', 'bandung', '1991-01-01', '2000-01-01', 1, '1', 1, 2, '2021-04-17 12:55:31', NULL);
INSERT INTO `table_employee` VALUES (3, 'RPJ880003', 'Dadang', 'bandung', '1991-01-01', '2000-02-02', 1, '1', 1, 2, '2021-04-17 12:56:03', NULL);
INSERT INTO `table_employee` VALUES (4, 'RPJ880004', 'Wawan', 'bandung', '1991-01-01', '2002-02-01', NULL, NULL, 3, 3, '2021-04-17 12:56:38', '2021-04-17 16:46:14');
INSERT INTO `table_employee` VALUES (5, 'RPJ880005', 'Diego', 'bandung', '1991-01-01', '2000-02-02', 3, '1 3', 1, 3, '2021-04-17 12:57:05', NULL);
INSERT INTO `table_employee` VALUES (6, 'RPJ880006', 'Yusron', 'bandung', '1991-01-01', '1991-01-01', 2, '1 2', 1, 3, '2021-04-17 12:57:46', '2021-04-17 16:46:14');
INSERT INTO `table_employee` VALUES (7, 'RPJ880007', 'Doni', 'bandung', '1991-01-01', '2002-01-01', 6, '1 2 6', 1, 4, '2021-04-17 12:58:16', '2021-04-17 16:47:35');

-- ----------------------------
-- Table structure for table_employee_mapping_parent
-- ----------------------------
DROP TABLE IF EXISTS `table_employee_mapping_parent`;
CREATE TABLE `table_employee_mapping_parent`  (
  `employee_id` int(10) UNSIGNED NOT NULL,
  `employee_parent_id` int(10) UNSIGNED NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of table_employee_mapping_parent
-- ----------------------------
INSERT INTO `table_employee_mapping_parent` VALUES (1, NULL);
INSERT INTO `table_employee_mapping_parent` VALUES (2, 1);
INSERT INTO `table_employee_mapping_parent` VALUES (3, 1);
INSERT INTO `table_employee_mapping_parent` VALUES (5, 1);
INSERT INTO `table_employee_mapping_parent` VALUES (5, 3);
INSERT INTO `table_employee_mapping_parent` VALUES (6, 1);
INSERT INTO `table_employee_mapping_parent` VALUES (6, 2);
INSERT INTO `table_employee_mapping_parent` VALUES (7, 1);
INSERT INTO `table_employee_mapping_parent` VALUES (7, 2);
INSERT INTO `table_employee_mapping_parent` VALUES (7, 6);

-- ----------------------------
-- Table structure for table_employee_status
-- ----------------------------
DROP TABLE IF EXISTS `table_employee_status`;
CREATE TABLE `table_employee_status`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of table_employee_status
-- ----------------------------
INSERT INTO `table_employee_status` VALUES (1, 'Tetap', '2021-04-17 12:53:26', NULL);
INSERT INTO `table_employee_status` VALUES (2, 'Kontrak', '2021-04-17 12:53:26', NULL);
INSERT INTO `table_employee_status` VALUES (3, 'Keluar', '2021-04-17 12:53:26', NULL);

-- ----------------------------
-- Table structure for table_lookup
-- ----------------------------
DROP TABLE IF EXISTS `table_lookup`;
CREATE TABLE `table_lookup`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of table_lookup
-- ----------------------------
INSERT INTO `table_lookup` VALUES (1, 'nip', 'format nip', '880007', '2021-04-17 12:53:26', '2021-04-17 12:58:16');

SET FOREIGN_KEY_CHECKS = 1;
