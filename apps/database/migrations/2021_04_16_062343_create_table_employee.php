<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_employee', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nip',150)->unique();
            $table->string('fullname',150);
            $table->string('place_of_birth',100)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->date('join_date')->nullable();
            $table->integer('employee_parent_id')->nullable();
            $table->string('list_employee_parent')->nullable();
            $table->foreignId('employee_status_id');
            $table->integer('level');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_employee');
    }
}
