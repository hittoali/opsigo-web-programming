<?php

function generateClassStatus($id)
{
	if ($id == 1) {
		return "label-light-success";
	} else if ($id == 2) {
		return "label-light-primary";
	} else {
		return "label-light-danger";
	}
}