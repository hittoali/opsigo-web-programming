<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeStatus extends Model
{
    protected $table = 'table_employee_status';

    protected $fillable = [
        'id','name','created_at','updated_at'
    ];

    public $timestamps = false;

    public static function boot()
    {
       parent::boot();

       static::creating(function($model)
       {
           $model->created_at = date("Y-m-d H:i:s");
       });

       static::updating(function($model)
       {
           $model->updated_at = date("Y-m-d H:i:s");
       });
   }
}
