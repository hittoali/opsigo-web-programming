<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'table_employee';

    protected $fillable = [
        'id','nip','fullname','place_of_birth','date_of_birth','join_date','employee_status_id','employee_parent_id','list_employee_parent','level','created_at','updated_at'
    ];

    public $timestamps = false;

    public static function boot()
    {
       parent::boot();

       static::creating(function($model)
       {
           $model->created_at = date("Y-m-d H:i:s");
       });

       static::updating(function($model)
       {
           $model->updated_at = date("Y-m-d H:i:s");
       });
   }

   public function status()
   {
      return $this->hasOne('App\Models\EmployeeStatus', 'id','employee_status_id');
   }

   public function parent()
   {
      return $this->hasOne('App\Models\Employee','id','employee_parent_id');
   }

   public function mapping()
   {
      return $this->hasMany('App\Models\EmployeeMapping','employee_parent_id','id');
   }

}
