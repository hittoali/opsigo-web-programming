<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeMapping extends Model
{
    protected $table = 'table_employee_mapping_parent';

    protected $fillable = [
        'employee_id','employee_parent_id'
    ];

    public $timestamps = false;

    public function employee()
   	{
    	return $this->hasOne('App\Models\Employee','id','employee_id');
   	}

}
