<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\EmployeeStatus;
use App\Models\EmployeeMapping;
use App\Models\Lookup;
use DB,Session;

class EmployeeController extends Controller
{
    protected $status_tetap;
    protected $status_keluar;
    protected $lookup_nip;
    protected $prefix_nip;
    protected $since;
    protected $director_id;

    public function __construct()
    {
        $this->status_tetap = 1;
        $this->status_keluar = 3;
        $this->lookup_nip = 1;
        $this->prefix_nip = "RPJ";
        $this->since = date('Y-m-d', strtotime('1980-02-15'));
        $this->director_id = 1;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modelEmployee = Employee::with('status','parent','mapping')->get();

        $data = [
            'employees' => $modelEmployee
        ];

        return view('app.employee.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modelStatus = EmployeeStatus::pluck('name','id');
        $modelParent = Employee::where('employee_status_id',$this->status_tetap)->pluck('fullname','id');
        $modelLookup = Lookup::where('id',$this->lookup_nip)->first();

        $data = [
            "nip" => $this->prefix_nip.((int)$modelLookup->value + 1),
            "status" => $modelStatus,
            "parent" => $modelParent
        ];

        return view('app.employee.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nip' => 'required',
            'fullname' => 'required',
            'place_of_birth' => 'required',
            'date_of_birth' => 'required|date',
            'join_date' => 'required|date|after_or_equal:'.$this->since,
            'employee_status_id' => 'required',
            'employee_parent_id' => 'required'
        ]);

        DB::beginTransaction();
        try {

            $modelParentEmployee = Employee::where([
                                        ['id','=',$request->employee_parent_id],
                                        ['employee_status_id','=',$this->status_tetap]
                                    ])->first();

            $dataParentEmployee = trim($modelParentEmployee->list_employee_parent." ".$request->employee_parent_id);

            $modelEmployee = new Employee;
            $modelEmployee->nip = $request->nip;
            $modelEmployee->fullname = $request->fullname;
            $modelEmployee->place_of_birth = $request->place_of_birth;
            $modelEmployee->date_of_birth = date('Y-m-d', strtotime($request->date_of_birth));
            $modelEmployee->join_date = date('Y-m-d', strtotime($request->join_date));
            $modelEmployee->employee_status_id = $request->employee_status_id;
            $modelEmployee->employee_parent_id = $request->employee_parent_id;
            $modelEmployee->list_employee_parent = $dataParentEmployee;
            $modelEmployee->level = (int)$modelParentEmployee->level + 1;
            $modelEmployee->save();

            $this->rebuildMapping();

            $modelLookup = Lookup::where('id',$this->lookup_nip)->first();
            $modelLookup->value = (int)$modelLookup->value + 1;
            $modelLookup->save();

            DB::commit();
            Session::flash('alert-success', 'Data karyawan berhasil di simpan');
            
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('alert-danger', 'Terjadi kesalahan, kontak support'. $e);
        }

        return redirect()->route('employee.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modelStatus = EmployeeStatus::pluck('name','id');
        $modelEmployee = Employee::where('id',$id)->first();
        $modelParent = Employee::where([
                                        ['employee_status_id','=',$this->status_tetap],
                                        ['level','<=',$modelEmployee->level],
                                        ['id','!=',$modelEmployee->id],
                                    ])->pluck('fullname','id');

        $data = [
            'employee' => $modelEmployee,
            'parent' => $modelParent,
            'status' => $modelStatus,
            'is_director' => ($modelEmployee->id == $this->director_id) ? true : false
        ];

        return view('app.employee.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'nip' => 'required',
            'fullname' => 'required',
            'place_of_birth' => 'required',
            'date_of_birth' => 'required|date',
            'join_date' => 'required|date|after_or_equal:'.$this->since,
            'employee_status_id' => 'required',
        ]);

        DB::beginTransaction();
        try {

            if ($this->director_id == $id) {
                $modelEmployee = Employee::where('id',$id)->first();
                $modelEmployee->fullname = $request->fullname;
                $modelEmployee->place_of_birth = $request->place_of_birth;
                $modelEmployee->date_of_birth = date('Y-m-d', strtotime($request->date_of_birth));
                $modelEmployee->join_date = date('Y-m-d', strtotime($request->join_date));
                $modelEmployee->save();
            } else {
                $modelParentEmployee = Employee::where([
                                            ['id','=',$request->employee_parent_id],
                                            ['employee_status_id','=',$this->status_tetap]
                                        ])->first();

                $dataParentEmployee = trim($modelParentEmployee->list_employee_parent." ".$request->employee_parent_id);

                $modelEmployee = Employee::with('mapping')->where('id',$id)->first();
                $modelEmployee->fullname = $request->fullname;
                $modelEmployee->place_of_birth = $request->place_of_birth;
                $modelEmployee->date_of_birth = date('Y-m-d', strtotime($request->date_of_birth));
                $modelEmployee->join_date = date('Y-m-d', strtotime($request->join_date));
                $modelEmployee->employee_status_id = $request->employee_status_id;
                $modelEmployee->employee_parent_id = $request->employee_parent_id;
                $modelEmployee->list_employee_parent = $dataParentEmployee;
                $modelEmployee->level = (int)$modelParentEmployee->level + 1;
                $modelEmployee->save();

                $this->rebuilListEmpolyeeParent($modelEmployee->toArray());
                $this->rebuildMapping();
            }


            DB::commit();
            Session::flash('alert-success', 'Data karyawan berhasil di ubah');
            
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('alert-danger', 'Terjadi kesalahan, kontak support'. $e);
        }

        return redirect()->route('employee.list');
    }

    /**
     * Terminate the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function terminate($id)
    {
        DB::beginTransaction();
        try {

            $modelEmployee = Employee::with('mapping')->where('id',$id)->first();
            $this->rebuilListEmpolyeeParentTerminate($modelEmployee->toArray());

            $modelEmployee->employee_status_id = $this->status_keluar;
            $modelEmployee->employee_parent_id = null;
            $modelEmployee->list_employee_parent = null;
            $modelEmployee->save();

            $this->rebuildMapping();

            DB::commit();
            Session::flash('alert-success', 'Data karyawan berhasil di ubah');
            
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('alert-danger', 'Terjadi kesalahan, kontak support'. $e);
        }

        return redirect()->route('employee.list');
    }

    public function rebuildMapping()
    {
        $modelEmployee = Employee::where([['employee_status_id','!=',$this->status_keluar]])
                        ->get()
                        ->toArray();

        $data_mapping_parent_employee = [];
        
        foreach ($modelEmployee as $k => $v) {
            $array_parent_employee = explode(" ",$v['list_employee_parent']);

            foreach ($array_parent_employee as $key => $value) {
                $data_mapping_parent_employee[] = [
                    "employee_id" => $v['id'],
                    "employee_parent_id" => empty($value) ? null : $value
                ];                
            }
        }

        EmployeeMapping::truncate();
        return EmployeeMapping::insert($data_mapping_parent_employee);
    }

    public function rebuilListEmpolyeeParent($data)
    {
        foreach ($data['mapping'] as $k => $v) {
            
            $modelParentEmployee = Employee::where([
                                        ['id','=',$v['employee_parent_id']],
                                        ['employee_status_id','=',$this->status_tetap]
                                    ])->first();

            $dataParentEmployee = trim($modelParentEmployee->list_employee_parent." ".$data['id']);

            $modelEmployee = Employee::where('id',$v['employee_id'])->first();
            $modelEmployee->list_employee_parent = $dataParentEmployee;
            $modelEmployee->level = (int)$modelParentEmployee->level + 1;
            $modelEmployee->save();

        }

        return true;
    }

    public function rebuilListEmpolyeeParentTerminate($data)
    {
        foreach ($data['mapping'] as $k => $v) {

            $modelEmployee = Employee::where('id',$v['employee_id'])->first();
            if ($modelEmployee->employee_parent_id == $data['id']) {
                $modelEmployee->employee_parent_id = $data['employee_parent_id'];
            }


            $modelParentEmployee = Employee::where([
                                        ['id','=',$modelEmployee->employee_parent_id],
                                        ['employee_status_id','=',$this->status_tetap]
                                    ])->first();



            $dataParentEmployee = trim($modelParentEmployee->list_employee_parent." ".$modelParentEmployee->id);
            $modelEmployee->list_employee_parent = $dataParentEmployee;
            $modelEmployee->level = (int)$modelParentEmployee->level + 1;
            $modelEmployee->save();

        }

        return true;
    }
}
