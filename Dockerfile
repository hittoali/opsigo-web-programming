FROM debian:stretch

ENV DEBIAN_FRONTEND noninteractive

# install NGINX
RUN apt-get update && \
	apt-get install -y nginx --no-install-recommends && \
	rm -rf /var/lib/apt/lists/*

# install
RUN apt-get update
RUN apt-get install -y \
     git \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg\
     software-properties-common 
RUN rm -rf /var/lib/apt/lists/*

RUN curl https://packages.sury.org/php/apt.gpg | apt-key add -
RUN echo 'deb https://packages.sury.org/php/ stretch main' > /etc/apt/sources.list.d/deb.sury.org.list

#RUN 7.3
RUN apt-get update && \
	apt-get install -y php7.3-fpm php7.3-cli php7.3-common php7.3-opcache php7.3-curl php7.3-mbstring php7.3-zip php7.3-xml php7.3-gd php7.3-mysql --no-install-recommends && \
	rm -rf /var/lib/apt/lists/*

	
# php-redis install
RUN apt-get update && \
	apt-get install -y php-redis --no-install-recommends && \
	rm -rf /var/lib/apt/lists/*

	
RUN useradd -ms /bin/bash nginx

# nano install
RUN apt-get update && \
	apt-get install -y nano --no-install-recommends && \
	rm -rf /var/lib/apt/lists/*

#Timezone
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Composer installation.
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN composer clear-cache
ENV PATH="${PATH}:/root/.composer/vendor/bin"

	
COPY php7proxy /etc/nginx/php7proxy
COPY nginx.conf /etc/nginx/
COPY fe.conf /etc/nginx/conf.d/fe.conf
COPY php.ini /etc/php/7.3/fpm/
COPY www.conf /etc/php/7.3/fpm/pool.d/


EXPOSE 8111

CMD /etc/init.d/php7.3-fpm start && nginx -g "daemon off;"